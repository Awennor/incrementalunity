﻿using System.Collections;
using System.Collections.Generic;
using pidroh.IncrementalExtension;
using Pidroh.IncrementalExtension;
using TMPro;
using UnityEngine;

public class Container : MonoBehaviour {

    public TextMeshProUGUI text;

    public Viewable Viewable { get; internal set; }
    public UINumbersAndTags Tags { get; internal set; }
    public bool tabVisible = true;
    internal bool viewableVisible;


    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
