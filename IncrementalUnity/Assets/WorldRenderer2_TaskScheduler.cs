﻿using IncrementalCore;
using Pidroh.IncrementalExtension;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Association = global::WorldRenderer2.Association;


public partial class WorldRenderer2
{

    public UnitRender activeAction;
    public UnitRender[] scheduledActions;
    public UnitRender[] doneActions;


    //public List<Task> tasks = new List<Task>();
    private List<Association> assocsWithTimer = new List<Association>();
    private Association active;
    List<Association> completed = new List<Association>();
    List<Association> scheduled = new List<Association>();

    void AddTask(Association a)
    {
        if (a.timer != null)
        {
            assocsWithTimer.Add(a);
        }
    }

    public void UpdateTaskScheduler()
    {
        foreach (var at in assocsWithTimer)
        {

            if (active == at && (active.timer.progress.Val >= 1 || active.timer.progress.Val == 0))
            {
                //Debug.Log("THIS HAPPENS... T-T");
                if (scheduled.Count > 0)
                {
                    //SetActive(scheduled[0]);
                    //scheduled.RemoveAt(0);
                    //UpdateSchedules();
                }
                else {
                }

                active = null;
                NoLabel(activeAction);

                completed.Add(at);
                UpdateCompleted();
                if (at.completed != null && at.completed.IsCompleted())
                {

                }
            }

            if (at.timer.progress.Val > 0)
            {
                if (at.timer.active)
                {
                    if (active != at)
                    {
                        SetActive(at);
                    }

                }
                else
                {
                    if (!scheduled.Contains(at))
                    {
                        scheduled.Add(at);
                        UpdateSchedules();
                    }
                }
               
            }
        }
        for (int i = 0; i < scheduled.Count; i++)
        {
            if (scheduled[i].timer.active)
            {
                scheduled.Remove(scheduled[i]);
                UpdateSchedules();
                i--;
            }
        }
        if (active != null)
        {
            var progress = active.timer.progress.Val;
            if (progress > 0)
                activeAction.image.transform.localScale = new Vector3(progress, 1);
        }
        else {
            if (scheduled.Count > 0)
            {
                Input(scheduled[0]);
                //SetActive(scheduled[0]);
            }
        }
    }

    private void SetActive(Association at)
    {
        active = at;
        Label(at, activeAction);
    }

    private void UpdateSchedules()
    {
        for (int i = 0; i < scheduledActions.Length; i++)
        {
            if (i < scheduled.Count)
            {
                Label(scheduled[i], scheduledActions[i]);
            }
            else
            {
                NoLabel(scheduledActions[i]);
            }
        }
    }

    private void UpdateCompleted()
    {
        for (int i = 0; i < doneActions.Length; i++)
        {
            if (i < completed.Count)
            {
                Label(completed[completed.Count - i - 1], doneActions[i]);
            }
            else
            {
                NoLabel(doneActions[i]);
            }
        }
    }

    public void InitTaskScheduler()
    {
        NoLabel(activeAction);
        UpdateCompleted();
        UpdateSchedules();
    }

    private void NoLabel(UnitRender uR)
    {
        //uR.mainText.text = string.Empty;
        uR.gameObject.SetActive(false);
    }

    private void Label(Association at, UnitRender ur)
    {
        ur.mainText.text = rmd.UnitLabels[at.u];
        ur.gameObject.SetActive(true);
    }
}

