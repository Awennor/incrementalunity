﻿using IncrementalCore;
using IncrementalExtension;
using Pidroh.IncrementalExtension;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Association = global::WorldRenderer2.Association;


public partial class WorldRenderer2
{

    public UnitRender firstTabButton;
    IncrementalExtension.IncrementalWorld2.UITags currentTag;
    Dictionary<UnitRender, IncrementalExtension.IncrementalWorld2.UITags>
        buttonToTag = new Dictionary<UnitRender, IncrementalExtension.IncrementalWorld2.UITags>();
    List<object> tabsToHide = new List<object>();
    [SerializeField]
    private Color activeTab;
    [SerializeField]
    private Color desactiveTab;
    List<object> tags = new List<object>();

    public void InitTabs()
    {

        var tabB = firstTabButton;

        Bind(IncrementalExtension.IncrementalWorld2.UITags.TabExplore, tabB);
        Bind(IncrementalExtension.IncrementalWorld2.UITags.TabHire, CreateTabButton());
        Bind(IncrementalExtension.IncrementalWorld2.UITags.TabDevelopment, CreateTabButton());
        ChangeTag(IncrementalExtension.IncrementalWorld2.UITags.TabExplore);
    }

    public void Update_Tabs()
    {
        tabsToHide.Clear();
        tabsToHide.AddRange(tags);
        foreach (var item in tabsToHide)
        {
            //Debug.Log("init "+item);
        }
    }

    public void Update_Tabs_AfterContainerCheck()
    {
        foreach (var item in tabsToHide)
        {
            //Debug.Log(item);
        }
        foreach (var item in buttonToTag)
        {
            if (tabsToHide.Contains(item.Value))
            {
                if (item.Key.gameObject.activeSelf)
                    item.Key.gameObject.SetActive(false);
            }
            else
            {
                if (!item.Key.gameObject.activeSelf)
                    item.Key.gameObject.SetActive(true);
            }
        }
    }

    public void CheckContainerTab(Container c)
    {
        if (c.Tags.HasWorld2Tag(currentTag))
        {

            if (c.viewableVisible)
            {
                tabsToHide.Remove(currentTag);
            }
            c.tabVisible = true;
        }
        else
        {
            foreach (var t in tags)
            {
                if (c.Tags.HasTag(t))
                {
                    c.tabVisible = false;
                    if (c.viewableVisible)
                    {
                        tabsToHide.Remove(t);
                    }


                }
            }

        }
    }

    private UnitRender CreateTabButton()
    {
        var button = Instantiate(firstTabButton, firstTabButton.transform.parent);
        return button;
    }

    private void Bind(IncrementalExtension.IncrementalWorld2.UITags tag, UnitRender tabB)
    {
        tags.Add(tag);
        buttonToTag.Add(tabB, tag);
        tabB.button.onClick.AddListener(() =>
        {
            ChangeTag(tag);
        });
        string tabName = "";
        switch (tag)
        {
            case IncrementalExtension.IncrementalWorld2.UITags.TabDevelopment:
                tabName = "Develop";
                break;
            case IncrementalExtension.IncrementalWorld2.UITags.TabExplore:
                tabName = "Explore";
                break;
            case IncrementalExtension.IncrementalWorld2.UITags.TabHire:
                tabName = "Hire";
                break;
            default:
                break;
        }
        tabB.mainText.text = tabName;
    }

    private void ChangeTag(IncrementalWorld2.UITags tag)
    {
        currentTag = tag;
        foreach (var item in buttonToTag)
        {

            if (item.Value == tag)
            {
                item.Key.mainText.color = activeTab;
            }
            else
            {
                item.Key.mainText.color = desactiveTab;
            }
        }
    }
}

