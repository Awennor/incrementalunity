﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class UnitRender : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{

    public TextMeshProUGUI mainText, mainText2, subText, subTextSmall;
    public Image image, frame;
    public Button button;
    public GameObject overlayParent;
    public TextMeshProUGUI overlaytext;
    public static UnitRender overlaid;
    public RectTransform rectTrasnform;
    static float timeToUnoverlay;
    internal bool textlessMode;

    public void OnPointerEnter(PointerEventData eventData)
    {


    }

    public void OnPointerExit(PointerEventData eventData)
    {
        timeToUnoverlay = 0.3f;
        //overlaid = null;
        //overlayParent.SetActive(false);
    }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        bool insideRect = RectTransformUtility.RectangleContainsScreenPoint(rectTrasnform, Input.mousePosition, null);
        if (this == UnitRender.overlaid && timeToUnoverlay > 0)
        {
            timeToUnoverlay -= Time.deltaTime;
            //Debug.Log(this.gameObject.name + " reducing overlay time");
            if (timeToUnoverlay <= 0)
            {
                overlaid = null;
                //Debug.Log(this.gameObject.name + " NOT OVERLAID");
            }

        }
        if (this == UnitRender.overlaid
            )
        {
            //Debug.Log(this.gameObject.name + " request unoverlay A");
            if (!insideRect)
            {
                //Debug.Log(this.gameObject.name + " request unoverlay B");
                if (timeToUnoverlay <= 0)
                {
                    
                    timeToUnoverlay = 0.3f;
                    //Debug.Log(this.gameObject.name + " request unoverlay");
                }
                
            }

        }
        if (overlayParent != null && this != UnitRender.overlaid && insideRect && this.gameObject.activeSelf)
        {
            //Debug.Log("ENTER HERE");
            overlaid = this;
            timeToUnoverlay = -1;
            //Debug.Log(this.gameObject.name);
            //overlayParent.SetActive(true);
            overlayParent.transform.position = this.transform.position;
        }
    }

    internal bool IsVisible()
    {
        if (!gameObject.activeInHierarchy) return false;
        return OnScreen();
    }

    public bool OnScreen()
    {
        return rectTrasnform.VisibleApprox_OverlayMode();
    }
}
