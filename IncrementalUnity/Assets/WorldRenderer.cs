﻿using IncrementalCore;
using IncrementalExtension;
using IncrementalExtension.Console;
using IncrementalTry;
using IncrementalTry.ConsoleInput;
using pidroh.IncrementalExtension;
using Pidroh.IncrementalExtension;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class WorldRenderer : MonoBehaviour
{
    List<Button> buttons = new List<Button>();
    List<AbstractRenderer> renders = new List<AbstractRenderer>();
    List<AbstractRenderer> renders2 = new List<AbstractRenderer>();
    List<InputableInput> inputs = new List<InputableInput>();
    public Text text;
    public Text textEvent;
    StringBuilder sb = new StringBuilder();
    StringBuilder aux = new StringBuilder();
    private IncrementalWorld w;
    public Button buttonPrefab;
    public GameObject buttonParent;
    private ReflectionWorldAnalysis rwa;
    private int numberOfEvents;
    Dictionary<Button, Happening> buttonToHappen = new Dictionary<Button, Happening>();


    // Use this for initialization
    void Start()
    {
        RandomSupplier.Generate = () => { return UnityEngine.Random.Range(0, 1f); };
        w = IncrementalWorld.CreateWorld();
        var world = w;

        TextScreen s = new TextScreen();
        rwa = new ReflectionWorldAnalysis(world, world.Rmd, s);
        var battleScreen = new BattleScreen(world).Screen;
        battleScreen.Add(world.BattleAreaAccess, world.Rmd, '+', "To the town plaza");
        var currentScreen = s;
        renders = s.Renders_info;
        renders2 = s.Renders_input;
        inputs = s.Inputs;
        world.BattleAreaAccessEvent.EventHappen += () =>
        {
            if (currentScreen == s)
            {
                currentScreen = battleScreen;
            }
            else
            {
                currentScreen = s;
            }
            var ss = currentScreen;
            //renders = ss.Renders_info;
            renders2 = ss.Renders_input;
            inputs = ss.Inputs;
        };

        world.extendedCore.CoreSystem.OnHappen += (h) => {
            if (world.Rmd.HappeningEventTexts.ContainsKey(h))
            {
                rwa.Events.Add(world.Rmd.HappeningEventTexts[h]);
            }

        };


    }

    // Update is called once per frame
    void Update()
    {
        w.SetValue(w.Time.Delta, UnityEngine.Time.deltaTime);
        w.Happen(w.Time.Frame);

        sb.Length = 0;

        //sb.AppendLine("##################################################");
        //sb.AppendLine(" Stats");
        sb.AppendLine();

        foreach (var r in renders)
        {
            sb.Append(' '); sb.Append(' ');
            r.Render(sb);
            sb.AppendLine();
        }


        while (buttonParent.transform.childCount < renders2.Count)
        {
            var button = Instantiate(buttonPrefab, buttonParent.transform);
            button.onClick.AddListener(() =>
            {
                ButtonPress(button);
            });
            buttons.Add(button);
        }
        for (int i = 0; i < buttons.Count; i++)
        {
            if (i < renders2.Count)
            {
                buttons[i].gameObject.SetActive(true);
                aux.Length = 0;
                renders2[i].Render(aux);
                aux.Replace(RenderConstants.DisabledText, "");
                aux.Replace(RenderConstants.EndMarkUp, "");
                if (aux.Length == 0)
                {
                    buttons[i].gameObject.SetActive(false);
                }
                else {
                    var text2 = buttons[i].transform.GetComponentInChildren<TextMeshProUGUI>();
                    if (text2 != null)
                    {
                        text2.text = aux.ToString();
                    }
                    else {
                        Text text1 = buttons[i].transform.GetComponentInChildren<Text>();
                        if (text1 != null)
                            text1.text = aux.ToString();
                    }
                    
                }
                
                
            }
            else {
                buttons[i].gameObject.SetActive(false);
            }
        }

        //sb.AppendLine("##################################################");
        //sb.AppendLine(" Actions");
        //sb.AppendLine();

        //foreach (var r in renders2)
        //{
        //    var l = sb.Length;
        //    sb.Append(' '); sb.Append(' ');
        //    r.Render(sb);
        //    if (l != sb.Length)
        //        sb.AppendLine();
        //}

        while (numberOfEvents != rwa.Events.Count)
        {
            textEvent.text = rwa.Events[numberOfEvents] + "\n\n" + textEvent.text;
            numberOfEvents++;
        }
        //sb.AppendLine("##################################################");
        //sb.AppendLine(" Events");
        //sb.AppendLine();
        //sb.AppendLine(RenderConstants.DisabledText);
        //var events = rwa.Events;
        //for (int i = 0; i < 5; i++)
        //{
        //    int index = events.Count - i - 1;
        //    if (index >= 0)
        //    {
        //        sb.AppendLine(events[index]);
        //        sb.AppendLine();
        //    }
        //}
        //sb.AppendLine(RenderConstants.EndMarkUp);
        var inputsLocal = this.inputs;
        for (int i2 = 0; i2 < inputsLocal.Count; i2++)
        {
            Button button1 = buttons[i2];
            button1.interactable = inputsLocal[i2].CanInput();
            buttonToHappen[button1] = inputsLocal[i2].Inputable.GetHappening();
            if (inputsLocal[i2].CanInput())
            {
                foreach (var c in Input.inputString)
                {
                    Debug.Log(c);
                    Debug.Log(inputsLocal[i2].Key);
                    if (inputsLocal[i2].Key == c)
                    {
                        Debug.Log("Happen!");
                        w.Happen(inputsLocal[i2].Inputable.GetHappening());
                    }
                }

            }
        }

        //Console.WriteLine(support.Val);
        string value = sb.ToString();
        value = value.Replace(RenderConstants.DisabledText, "<color=#7a5457>");
        value = value.Replace(RenderConstants.EndMarkUp, "</color>");
        if (value != text.text)
        {
            text.text = value;
        }
    }

    private void ButtonPress(Button button)
    {
        var h = buttonToHappen.ContainsKey(button) ? buttonToHappen[button] : null;
        if (h != null)
            w.Happen(h);
    }
}
