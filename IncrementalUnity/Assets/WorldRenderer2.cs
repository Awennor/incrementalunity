﻿using IncrementalCore;
using IncrementalExtension;
using pidroh.IncrementalExtension;
using pidroh.IncrementalExtension.UI;
using Pidroh.IncrementalExtension;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using TMPro;
using UnityEngine;
using UnityEngine.Analytics;
using UnityEngine.UI;


public partial class WorldRenderer2 : MonoBehaviour
{

    public UnitRender unitRenderPrefab;
    public UnitRender unitRenderPrefab_Big;
    public GameObject unitRenderParent;
    public GameObject unitRenderParent_Big;
    List<UnitRender> unitRenders = new List<UnitRender>();
    private RenderingMetaData rmd;
    Dictionary<UnitRender, Association> assocs = new Dictionary<UnitRender, Association>();
    private IncrementalWorld2 iw2;
    private float timeMultiplier = 1f;
    public Color defaultImageColor, battleImageColor, completedImageColor, disabledImageColor, defaultFrameColor, disabledFrameColor, timerImageColor, timerImagePausedColor, defaultTextColor, disabledTextColor, completedTextColor;
    public Text eventText;
    private EventManager eventManager;
    int nEvents;
    [SerializeField] List<ContainerPrefabWithList> containerPrefabs;
    public SaveSystem saveSystem;
    Dictionary<Happening, string> timerCompleteToLabel = new Dictionary<Happening, string>();
    public bool renderVisibleOptimization = true;
    public bool renderVisibleOptimization2  = true;

    // Use this for initialization
    void Start()
    {
        InitTabs();
        InitTaskScheduler();
        saveSystem.Setup();
        RandomSupplier.Generate = () => { return UnityEngine.Random.Range(0, 1f); };
        iw2 = new IncrementalWorld2();
        var world = iw2;
        iw2.Create();
        UnitWorld uw = iw2.UnitWorld;
        AreaManager areaManager = new AreaManager(uw, world.Core.CoreSystem);
        rmd = iw2.Rmd;
        areaManager.OnChangeArea += AreaManager_OnChangeArea;
        AreaManager_OnChangeArea(areaManager.currentArea);
        eventManager = new EventManager(iw2.Core.CoreSystem, rmd);
        saveSystem.WorldCreated(world.Core.CoreSystem);
        saveSystem.Load();

        world.Core.CoreSystem.OnHappen += (h) =>
        {
            var dic = new Dictionary<string, object>();
            string label;
            if (timerCompleteToLabel.TryGetValue(h, out label))
            {
                dic.Clear();
                dic.Add("label", label);
                Analytics.CustomEvent("taskcomplete", dic);
                Debug.Log("ANALYTICS");
            }

        };
    }

    private void AreaManager_OnChangeArea(Area obj)
    {
        for (int i = 0; i < unitRenders.Count; i++)
        {
            unitRenders[i].gameObject.SetActive(false);
            assocs[unitRenders[i]].u = null;
        }
        var areaUnit = obj.Unit;
        var container = areaUnit.GetComponent<UnitContainer>();


        UnitRender.overlaid = null;
        int unitRenderId = 0;

        var units = container.Units;
        unitRenderId = RecursiveUnitAssoc(unitRenderId, units, unitRenderParent, textlessMode: false);

    }

    private int RecursiveUnitAssoc(int unitRenderId, List<ExtendedCoreUnit> units, GameObject parent, bool textlessMode)
    {
        for (int i2 = 0; i2 < units.Count; i2++)
        {
            ExtendedCoreUnit u = units[i2];
            var uc = u.GetComponent<UnitContainer>();
            if (uc == null)
            {
                unitRenderId = Associate(unitRenderId, u, parent, textlessMode: textlessMode);
            }
            else
            {
                var container = containerPrefabs[0].GetContainer(parent);
                container.gameObject.name = u.Entity.Id;
                container.Viewable = u.GetComponent<Viewable>();
                container.Tags = u.GetComponent<UINumbersAndTags>();
                container.text.text = u.Entity.Id;
                string value;
                if (rmd.UnitLabels.TryGetValue(u, out value))
                {
                    container.text.text = value;
                }

                Debug.Log("recursive");
                unitRenderId = RecursiveUnitAssoc(unitRenderId, uc.Units, container.gameObject, textlessMode: false);
            }
        }

        return unitRenderId;
    }

    private int Associate(int unitRenderId, ExtendedCoreUnit u, GameObject parent, bool textlessMode)
    {
        while (unitRenderId >= unitRenders.Count)
        {
            if (u.GetComponent<UINumbersAndTags>().HasTag(IncrementalWorld2.UITags.ImportantUnit))
            {
                CreateNewUnitRender(unitRenderPrefab_Big, unitRenderParent_Big);
            }
            else
            {
                CreateNewUnitRender(unitRenderPrefab, unitRenderParent);
            }

        }
        UnitRender render = unitRenders[unitRenderId];
        if (render.transform.parent != unitRenderParent_Big.transform)
            render.transform.SetParent(parent.transform);
        render.gameObject.SetActive(true);
        Associate(render, u);
        render.textlessMode = textlessMode;

        unitRenderId++;
        return unitRenderId;
    }

    private void Associate(UnitRender render, ExtendedCoreUnit u)
    {

        var ass = assocs[render];
        ass.u = u;
        string value;

        u.TryGetComponent(out ass.inputable);
        u.TryGetComponent(out ass.viewable);
        u.TryGetComponent(out ass.buyable);
        u.TryGetComponent(out ass.timer);
        u.TryGetComponent(out ass.valueMax);
        u.TryGetComponent(out ass.numbTags);
        u.TryGetComponent(out ass.completed);
        u.TryGetComponent(out ass.conditions);
        u.TryGetComponent(out ass.valueChanger);

        AddTask(ass);

        string label;
        var task = ass.timer;
        rmd.UnitLabels.TryGetValue(u, out label);
        if (label != null && task != null)
        {
            timerCompleteToLabel.Add(task.willTrigger, label);
        }



        ass.value = u.GetValue();
        ass.formula = u.GetFormula();
        //Debug.Log(ass.value);
        render.gameObject.SetActive(true);
        if (rmd.UnitLabels.TryGetValue(u, out value))
        {
        }
        else
        {
            value = u.Entity.Id;
        }
        var textMeshPro = render.mainText;
        textMeshPro.text = value;
        ass.mainLabel = value;
    }

    private void CreateNewUnitRender(UnitRender prefab, GameObject parent)
    {
        var uRender = Instantiate(prefab, parent.transform);
        uRender.gameObject.name = "" + unitRenders.Count;
        var button = uRender.GetComponentInChildren<Button>();
        button.onClick.AddListener(() => { UnitButtonPressed(uRender); });
        Association value1 = new Association();
        value1.render = uRender;
        assocs.Add(uRender, value1);
        unitRenders.Add(uRender);
        uRender.gameObject.SetActive(false);
    }

    private void UnitButtonPressed(UnitRender uRender)
    {
        if (assocs[uRender].inputable != null)
        {
            Input(uRender);
        }
    }

    private void Input(UnitRender uRender)
    {
        Association association = assocs[uRender];
        Input(association);
    }

    private void Input(Association association)
    {
        if (association.inputable.CanInput())
            iw2.Happen(association.inputable.GetHappening());
    }

    [Serializable]
    internal class Association
    {
        public UnitRender render;
        public ExtendedCoreUnit u;
        public Inputable inputable;
        public Viewable viewable;
        public Buyable buyable;
        public Value value;
        public Formula formula;
        public Timer timer;
        public ValueMax valueMax;
        public Completed completed;
        public ValueChangerHolder valueChanger;
        public UINumbersAndTags numbTags;
        internal string mainLabel;
        public ConditionHolder conditions;
    }



    // Update is called once per frame
    void Update()
    {

        UpdateTaskScheduler();
        iw2.SetDelta(UnityEngine.Time.deltaTime * timeMultiplier);
        iw2.TimeFrame();

        Update_Tabs();

        if (Application.isEditor)
        {
            if (UnityEngine.Input.GetKeyDown(KeyCode.T))
            {
                int v = 200;
                if (timeMultiplier == v)
                    timeMultiplier = 1;
                else
                    timeMultiplier = v;
            }
            if (UnityEngine.Input.GetKeyDown(KeyCode.Y))
            {
                if (timeMultiplier == 3)
                    timeMultiplier = 1;
                else
                    timeMultiplier = 3;

            }
        }
        while (nEvents < eventManager.Events.Count)
        {
            
            string text = eventManager.Events[nEvents] + "\n\n" + eventText.text;
            if (text.Length > 4000) {
                text = text.Substring(0, 2000);
            }
            eventText.text = text;
            nEvents++;


        }

        foreach (var item in this.containerPrefabs)
        {
            foreach (var item2 in item.containersCreated)
            {

                if (item2.Viewable != null)
                {
                    item2.viewableVisible = item2.Viewable.CanView();
                }
                else
                {
                    item2.viewableVisible = true;
                }

                CheckContainerTab(item2);
                item2.gameObject.SetActive(item2.viewableVisible && item2.tabVisible);
            }
        }
        Update_Tabs_AfterContainerCheck();

        foreach (var item in assocs.Values)
        {
            if (UnitRender.overlaid == null)
            {
                if (item.render.overlayParent.activeSelf)
                    item.render.overlayParent.SetActive(false);
            }
            //Debug.Log("WWW2");
            if (item.u == null) continue;
            //Debug.Log("WWW");
            if (item.render.OnScreen() || !renderVisibleOptimization2)
            {
                if (item.viewable != null)
                {
                    if (item.render.gameObject.activeSelf != item.viewable.CanView())
                        item.render.gameObject.SetActive(item.viewable.CanView());
                }
            }
            else
            {
                continue;
            }
            

            if (renderVisibleOptimization && !item.render.IsVisible())
            {
                continue;
            }
            string description;
            rmd.ObjDescriptions.TryGetValue(item.u, out description);
            var textMain2 = item.render.mainText2.text;
            Color imageColor = defaultImageColor;
            Color frameColor = defaultFrameColor;
            Color mainTextColor = defaultTextColor;
            bool paused = false;
            bool IsOverlaid = UnitRender.overlaid == item.render;

            //item.render.mainText.text = item.u.Entity.Id;
            var mainText2 = "";
            var subtext = "";
            var subtextsmall = "";
            var overlayText = "";
            bool hideImage = false;
            if (item.value != null)
            {
                //Debug.Log("...");
                mainText2 += (int)item.value.Val;
            }

            if (item.formula != null)
            {
                //Debug.Log("...");
                mainText2 += (int)item.formula.ReadResult();
            }
            if (item.valueMax != null)
            {
                subtext = "  /  " + item.valueMax.Max.ReadResult();
            }
            if (item.buyable != null && IsOverlaid)
            {
                overlayText += rmd.ValueLabels[item.buyable.Currency] + ": " + (int)item.buyable.Cost.ReadResult();
                //mainText2 = "Cost: " + (int)item.buyable.Cost.ReadResult();
            }
            if (!textMain2.Equals(mainText2))
            {
                item.render.mainText2.text = mainText2;
            }



            if (IsOverlaid) {
                if (item.valueChanger != null)
                {
                    foreach (var vc in item.valueChanger.Changers)
                    {
                        var number = (int)vc.OperationValue.ReadResult();
                        if (number > 0)
                            overlayText += rmd.ValueLabels[vc.Value] + " +" + number + "\n";
                        if (number < 0)
                            overlayText += rmd.ValueLabels[vc.Value] + ":" + (-number) + "\n";
                    }
                }
            }
            
            bool validInput = true;
            if (item.numbTags != null )
            {
                foreach (var n in item.numbTags.Numbers)
                {
                    if (IsOverlaid) {
                        if (n.HasWorld2Tag(IncrementalWorld2.UITags.BattleVictoryChance))
                        {
                            overlayText += "Victory Chance: ";
                            overlayText += "<color=#dd0000>";
                            overlayText += (int)(n.Formula.ReadResult() * 100) + "%";
                            overlayText += "</color>";
                            overlayText += "\n";
                        }
                        if (n.HasWorld2Tag(IncrementalWorld2.UITags.TimeRate))
                        {
                            overlayText += rmd.ValueLabels[n.RelevantValue] + ": " + n.Formula.ReadResult() + "/s" + "\n";
                        }
                    }
                    
                    if (n.HasWorld2Tag(IncrementalWorld2.UITags.ValidInput))
                    {
                        validInput = n.Formula.ReadResult() == Formula.TRUE;
                    }

                }
                if (item.numbTags.HasWorld2Tag(IncrementalWorld2.UITags.Battle))
                {
                    imageColor = battleImageColor;
                }


            }




            
            if (item.render.textlessMode )
            {
                if (item.render.mainText.gameObject.activeSelf)
                    item.render.mainText.gameObject.SetActive(false);
                overlayText = item.mainLabel + "\n" + overlayText;
            }
            else
            {
                if (!item.render.mainText.gameObject.activeSelf)
                    item.render.mainText.gameObject.SetActive(true);
            }



            if (item.inputable != null)
            {
                if (!item.render.image.gameObject.activeSelf)
                    item.render.image.gameObject.SetActive(true);
                if (!item.render.frame.gameObject.activeSelf)
                    item.render.frame.gameObject.SetActive(true);

                item.render.button.interactable = item.inputable.CanInput();
                if (item.inputable.CanInput())
                {
                }
                else
                {

                    //is now controlled by ValidInput bool
                    //imageColor = disabledImageColor;
                    //frameColor = disabledFrameColor;
                    //mainTextColor = disabledTextColor;
                }
                if (!validInput)
                {
                    imageColor = disabledImageColor;
                    frameColor = disabledFrameColor;
                    mainTextColor = disabledTextColor;
                }
            }
            else
            {
                hideImage = true;
                if (!item.render.frame.gameObject.activeSelf)
                    item.render.frame.gameObject.SetActive(false);
                //item.render.image.gameObject.SetActive(false);
            }

            if (item.timer != null)
            {
                if(IsOverlaid)
                    overlayText += "Time: " + item.timer.TimeToComplete.ReadResult() + "\n";
                float progress = item.timer.progress.Val;
                if (progress > 0)
                    item.render.image.transform.localScale = new Vector3(progress, 1);
                else
                    item.render.image.transform.localScale = new Vector3(1, 1);
                if (item.timer.active)
                {


                    imageColor = timerImageColor;
                    frameColor = disabledFrameColor;
                    mainTextColor = defaultFrameColor;
                }
                else
                {
                    if (progress > 0)
                    {
                        imageColor = timerImagePausedColor;
                        paused = true;
                    }
                }
            }
            else
            {
                item.render.image.transform.localScale = new Vector3(1, 1);
            }

            if (item.conditions != null && IsOverlaid)
            {
                string conditionText = string.Empty;

                foreach (var con in item.conditions.Conditions)
                {

                    if (!con.Status.AsBool())
                    {
                        if (con.HasTag(IncrementalWorld2.UITags.Con_BattlePowerNotZero))
                        {
                            //conditionText += "Armed forces\n";
                            continue;
                        }
                        if (con.HasTag(IncrementalWorld2.UITags.Con_BattlePowerMinimum))
                        {
                            conditionText += "Fighting now would be suicide.";
                            continue;
                        }
                        if (con.HasTag(IncrementalWorld2.UITags.Con_MaxedResource))
                        {
                            conditionText += "Maxed.";
                            continue;
                        }

                        conditionText += rmd.ValueLabels[con.ValueTested] + ": " + (int)con.ValueTester.AsFloat() + "\n";
                    }
                }
                if (conditionText.Length > 0)
                {
                    conditionText =
                        //"<b>UNAVAILABLE </b>\n" +
                        "<color=red>" +
                        conditionText + "</color>\n";
                    overlayText = conditionText;
                }
            }

            if (description != null)
            {
                overlayText = description + "\n" + overlayText;
            }

            if (paused) overlayText = "Paused";
            if (item.completed != null && item.completed.IsCompleted())
            {
                imageColor = completedImageColor;
                mainTextColor = completedTextColor;

                overlayText = "";
            }

            IsOverlaid = UnitRender.overlaid == item.render;

            if (IsOverlaid)
            {
                if (overlayText.Length == 0)
                {
                    if (item.render.overlayParent.activeSelf)
                        item.render.overlayParent.SetActive(false);
                }
                else
                {
                    if (!item.render.overlayParent.activeSelf)
                        item.render.overlayParent.SetActive(true);
                    overlayText = overlayText.Trim();
                    if (overlayText != item.render.overlaytext.text)
                    {
                        item.render.overlaytext.text = overlayText;
                        Canvas.ForceUpdateCanvases();

                    }
                }
            }



            if (item.render.subTextSmall != null && subtextsmall != item.render.subTextSmall.text)
            {
                item.render.subTextSmall.text = subtextsmall;
            }
            item.render.image.color = imageColor;
            if (hideImage)
            {
                item.render.image.color = Color.clear;
            }
            if (subtext != item.render.subText.text)
            {
                item.render.subText.text = subtext;
            }
            item.render.mainText.color = mainTextColor;
            item.render.frame.color = frameColor;
        }
        //end
    }

    [Serializable]
    class ContainerPrefabWithList
    {
        public Container prefab;
        public List<Container> containersCreated = new List<Container>();
        int inUse = 0;

        internal Container GetContainer(GameObject parent)
        {
            if (containersCreated.Count <= inUse)
            {
                containersCreated.Add(Instantiate(prefab));
            }

            var gameObject1 = containersCreated[inUse];
            gameObject1.transform.SetParent(parent.transform);
            inUse++;
            return gameObject1;
        }
    }
}
