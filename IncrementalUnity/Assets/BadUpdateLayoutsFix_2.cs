﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BadUpdateLayoutsFix_2 : MonoBehaviour {
    private VerticalLayoutGroup[] vlgs;
    private HorizontalLayoutGroup[] hlgs;
    public float applyTime = 0.2f;
    private float applyTimeP;

    // Use this for initialization
    void Start () {

        vlgs = GetComponentsInChildren<VerticalLayoutGroup>(false);
        hlgs = GetComponentsInChildren<HorizontalLayoutGroup>(false);
    }
	
	// Update is called once per frame
	void Update () {
        if (applyTimeP >= 0)
        {
            applyTimeP -= Time.deltaTime;
            if (applyTimeP < 0) {
                applyTimeP = applyTime;
                foreach (HorizontalOrVerticalLayoutGroup lg in vlgs)
                {
                    Fix(lg);

                }
                foreach (HorizontalOrVerticalLayoutGroup lg in hlgs)
                {
                    Fix(lg);
                }
            }
        }
        

    }

    private static void Fix(HorizontalOrVerticalLayoutGroup lg)
    {
        if (lg != null && lg.enabled)
        {
            lg.enabled = !lg.enabled;
            lg.enabled = !lg.enabled;
            lg.enabled = !lg.enabled;
            lg.enabled = !lg.enabled;
            lg.enabled = true;
        }
        else
        {
            //Debug.Log("null");
        }
    }
}
