﻿using IncrementalCore;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveSystem : MonoBehaviour
{
    private AllValues allValues;
    DataAccess data;
    private PersistentData pData;
    private float timeToSave;

    public void Setup()
    {
        allValues = new AllValues();
        data = new DataAccess();
        pData = data.Load<PersistentData>();
        if (pData == null) pData = new PersistentData();
        pData.Report = (s) => { Debug.Log(s); };
    }

    public void WorldCreated(CoreSystem core)
    {
        pData.Core = core;
        pData.Setup(allValues);
    }

    public void Load()
    {
        pData.CopyToValues();
        
    }

    [ContextMenu("save")]
    public void Save()
    {
        pData.CopyFromValues();
        data.Save<PersistentData>(pData);
    }

    [ContextMenu("reset")]
    public void Reset()
    {
        pData = new PersistentData();
        Save();
    }

    private void OnApplicationQuit()
    {
        Save();
    }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        timeToSave -= Time.deltaTime;
        if (timeToSave < 0) {
            Save();
            timeToSave = 5;
        }
    }
}
